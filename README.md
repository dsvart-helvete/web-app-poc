# web-app-poc

Creacion del sitio web POC.

Para que se pueda ejecutar el proyecto se necesita tener instalado:
1. node.js
2. npm

Para inicializar el proyecto de node.js e instalar los modulos, se debe 
ejecutar en la terminal:

```
./init.sh
```

Cuando se instalen todos los modulos, ejecutar en terminales distintas 
los comandos:

```
npm run webpack
npm start
```